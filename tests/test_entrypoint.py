import importlib
from src_hello_world.entrypoint import run


def test_exposed():
    """Test package level exposed apis."""
    package = importlib.import_module("src_hello_world")
    assert hasattr(package, "__version__")
    assert hasattr(package, "__author__")


def test_welcome_message():
    """Test entrypoint function with welcome message"""
    wlc_msg = run()
    assert wlc_msg == "Hello Python Package!"


# def test_welcome_message2():
#     """Test entrypoint function with welcome message"""
#     wlc_msg = run()
#     assert wlc_msg == "Hello World!"
