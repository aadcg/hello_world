"""Top level file of your program"""


def run():
    """
    Default entrypoint function which initiates the package code.  Usually
    contains the CLI options. The current version returns a message.

        rtype: String

    """
    return "Hello Python Package!"
