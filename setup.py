"""Setup hello_world as a package."""

import setuptools

setuptools.setup(
    name="hello_world",
    description="This is the description of this package",
    version="0.1.0",
    url="git@gitlab.com:aadcg/hello_world.git",
    author="André Gomes",
    author_email="andremegafone@gmail.com",
    packages=["src_hello_world"],
    # add here the python libraries you need for your program when you're done
    # with the development!
    install_requires=[
        # "pandas==0.23.0",
        # "numpy==1.14.3",
        # "scipy==1.1.0"
    ],
    entry_points={
        "console_scripts": [
            "run_hello_world=src_hello_world.entrypoint:run"
        ]
    },
)
